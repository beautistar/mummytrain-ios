//
//  CommonUtils.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Const.h"
#import "AppDelegate.h"
#import "UserDefault.h"
#import "NSString+Encode.h"


@interface CommonUtils : NSObject

+ (BOOL) isValidEmail: (NSString *) email;

+(NSString *) saveToFile:(UIImage *) scrImage;

+ (void) saveUserInfo;
+ (void) loadUserInfo;

+ (void) setUserIdx:(int) idx;
+ (int) getUserIdx;


+ (void) setFirstName: (NSString *) firstname;
+ (NSString *) getFirstName;

+ (void) setSurName: (NSString *) surname;
+ (NSString *) getSurName;

+ (void) setUserEmail: (NSString *) email;
+ (NSString *) getUserEmail;

+ (void) setPhotoUrl: (NSString *) photo_url;
+ (NSString *) getPhotoUrl;

+ (void) setUserRegister:(BOOL) isRegistered;
+ (BOOL) getUserRegister;

@end
