//
//  CommonUtils.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "CommonUtils.h"
#import "UIImage+ResizeMagick.h"
#import "PrefConst.h"

@implementation CommonUtils

// valid email address checking

+ (BOOL) isValidEmail: (NSString *) email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage resizeTo:(CGSize) newSize {
    
    UIImage * resImage;
    
    resImage = [srcImage resizedImageByMagick:@"128x128#"];
//    resImage = [srcImage resizedImageByMagick:@"512x128#"];
    return resImage;
}

// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    outputFileName = @"profile.jpg";
    outputImage = [CommonUtils imageResize:srcImage resizeTo:CGSizeMake(PROFILE_IMG_SIZE, PROFILE_IMG_SIZE)];
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}

+ (void) saveUserInfo {
    
    [CommonUtils setUserIdx:APPDELEGATE.Me._idx];
    [CommonUtils setFirstName:APPDELEGATE.Me._firstname];
    [CommonUtils setSurName:APPDELEGATE.Me._surname];
    [CommonUtils setUserEmail:APPDELEGATE.Me._email];
    [CommonUtils setPhotoUrl:APPDELEGATE.Me._photoUrl];    
}

+ (void) loadUserInfo {
    
    if(APPDELEGATE.Me) {
        
        APPDELEGATE.Me._idx = [CommonUtils getUserIdx];
        APPDELEGATE.Me._firstname = [CommonUtils getFirstName];
        APPDELEGATE.Me._email = [CommonUtils getUserEmail];
        APPDELEGATE.Me._surname = [CommonUtils getSurName];
        APPDELEGATE.Me._photoUrl = [CommonUtils getPhotoUrl];
        
    }
}

+ (void) setUserIdx:(int)idx {
    
    [UserDefault setIntValue:PREFKEY_ID value:idx];
}

+ (int) getUserIdx {
    
    return [UserDefault getIntValue:PREFKEY_ID];
}

+ (void) setFirstName: (NSString *) firstname {
    
    [UserDefault setStringValue:PREFKEY_FIRSTNAME value:firstname];
}

+ (NSString *) getFirstName {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_FIRSTNAME];
    return res == nil ? @"" : res;
}

+ (void) setSurName: (NSString *) surname {
    
    [UserDefault setStringValue:PREFKEY_SURNAME value:surname];
    
}
+ (NSString *) getSurName {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_SURNAME];
    return res == nil ? @"" : res;
    
}

+ (void) setUserEmail: (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_EMAIL value:email];
    
}
+ (NSString *) getUserEmail {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_EMAIL];
    return res == nil ? @"" : res;
    
}

+ (void) setPhotoUrl: (NSString *) photo_url {
    
    [UserDefault setStringValue:PREFKEY_PHORO_URL value:photo_url];
    
}
+ (NSString *) getPhotoUrl {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_PHORO_URL];
    return res == nil ? @"" : res;
    
}

+ (void) setUserRegister:(BOOL) isRegistered {
    
    [UserDefault setBoolValue:PREFKEY_REGISTERED value:isRegistered];
    
}
+ (BOOL) getUserRegister {
    
    return [UserDefault getBoolValue:PREFKEY_REGISTERED];
    
}




@end
