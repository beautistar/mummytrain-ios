//
//  NSString+Encode.h
//  CampusGlue
//
//  Created by victory on 3/23/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(encode)

-(NSString *)encodeString:(NSStringEncoding) encoding;

@end
