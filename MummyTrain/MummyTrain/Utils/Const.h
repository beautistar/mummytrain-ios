//
//  Const.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JLToast/JLToast-Swift.h>
@import AFNetworking;
@import JLToast;

@interface Const : NSObject

extern NSString * SelStartTime;
extern NSString * SelEndTime;

extern int FromDetail;
extern int FromEdit;


/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

// -------------------------------------------------------------
// string define
// -------------------------------------------------------------

// alert common
#define ALERT_TITLE                 @"MummyTrain"
#define   ALERT_OK                  @"OK"
#define   ALERT_CANCEL              @"CANCEL"

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])


#define START_TIME_SELECT                   1
#define END_TIME_SELECT                     0

#define FROM_DETAIL                         1
#define FROM_OTHER                          0
#define FROM_EDIT                           10


#define FROM_MYEVENT                        100
#define FROM_WHATSON                        200

#define IS_GOING                            1
#define IS_CANTGO                           0
#define IS_MAYBE                            2
#define IS_DEFAULT                          -1

#define PROFILE_IMG_SIZE                    256
#define SAVE_ROOT_PATH              @"MummyTrain"

/**
 **
 **  string define
 **/

#define INPUT_FIRSTNAME             @"Please input your firstname."
#define INPUT_SURNAMR               @"Please input your surnamename."
#define INPUT_EMAIL                 @"Please input your email address."
#define INPUT_CORRECT_EMAIL         @"Please input your email address correctly."
#define INPUT_PWD                   @"Please input your password."
#define SELECT_PHOTO                @"Please select your photo."

#define EXIST_USERNAME              @"User name already exist."
#define EXIST_EMAIL                 @"Email already exist."
#define REGISTER_SUCCESS            @"Register success!"
#define PHOTO_UPLOAD_FAIL           @"Photo upload failed."
#define EVENT_TIME_ERROR            @"Selected event time was passed. \n please select the time again."
#define UPDATE_SUCCESS              @"Update success!"

#define INPUT_EVENTTITLE            @"Please input the event title."
#define SELECT_DATE                 @"Please select the event date"
#define SELECT_STARTTIME            @"Please select the event start time."
#define SELECT_ENDTIME              @"Please select the end time."
#define SELECT_LOCATION             @"Please select the location."
#define INPUT_DESCRIPTION           @"Please input about the event in detail."
#define WRITE_SONETHING             @"Please write something."

#define NEW_EVENT_CREATED           @"New event was created."
//
#define SEL_ATTEND                  @"Please select the attend status."

#define ATTEND_SUCCESS              @"Attending event was requested successfully."

#define UNREGISTERED                @"Email doesn't registered."
#define WRONG_PASSWORD              @"Wrong password"


#define CONN_ERROR                  @"Connecting to the server failed.\nPlease try again."

#define RES_ALREADYATTEND           @"You are already attending this event"

#define CONF_CANCEL                 @"Are you sure want to cancel this event?"



@end
