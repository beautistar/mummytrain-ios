//
//  PrefConst.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_ID              @"user_id"
#define PREFKEY_FIRSTNAME       @"firstname"
#define PREFKEY_SURNAME         @"surname"
#define PREFKEY_EMAIL           @"user_email"
#define PREFKEY_PHORO_URL       @"user_photourl"
#define PREFKEY_REGISTERED       @"user_registered"


#endif /* PrefConst_h */
