//
//  CommentEntity.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 06/07/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentEntity : NSObject

@property (nonatomic) int _idx;

@property (nonatomic, strong) NSString * _comment;

@property (nonatomic, strong) NSString * _photoUrl;

@property (nonatomic, strong) NSString *_dateTime;

@end
