//
//  CommentEntity.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 06/07/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "CommentEntity.h"

@implementation CommentEntity

@synthesize _idx, _comment, _photoUrl, _dateTime;

- (instancetype) init {
    
    if (self = [super init]){
        
        _idx = 0;
        _comment = @"";
        _photoUrl = @"";
        _dateTime = @"";
        
    }
    
    return self;
}

@end
