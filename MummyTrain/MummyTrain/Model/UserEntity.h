//
//  UserEntity.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEntity : NSObject

@property (nonatomic) int _idx;

@property (nonatomic, strong) NSString *_firstname;

@property (nonatomic, strong) NSString *_surname;

@property (nonatomic, strong) NSString *_email;

@property (nonatomic, strong) NSString * _photoUrl;

@property (nonatomic) int _status;

@end
