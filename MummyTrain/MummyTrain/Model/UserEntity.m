//
//  UserEntity.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize _idx, _surname, _firstname, _email, _photoUrl, _status;

- (instancetype) init {
    
    if (self = [super init]){
        
        _idx = 0;
        _firstname = @"";
        _surname = @"";
        _email = @"";
        _photoUrl = @"";
        _status = -1;
        
    }
    
    return self;
}

@end
