//
//  EventEntity.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 05/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventEntity : NSObject

@property (nonatomic) int _idx;

@property (nonatomic) int _user_id;

@property (nonatomic, strong) NSString *_title;

@property (nonatomic, strong) NSString *_date;

@property (nonatomic, strong) NSString *_startTime;

@property (nonatomic, strong) NSString *_endTime;

@property (nonatomic, strong) NSString *_detail;

@property (nonatomic, strong) NSString *_location;

@property (nonatomic, strong) NSString *_organiser;

@property (nonatomic) int _status;

@property (nonatomic) int _cancelled;

@property (nonatomic, strong) NSString * _photoUrl;

@end
