//
//  EventEntity.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 05/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "EventEntity.h"

@implementation EventEntity

@synthesize _idx, _title, _date, _startTime, _endTime, _location, _detail, _status, _photoUrl, _organiser;

- (instancetype) init {
    
    if (self = [super init]){
        
        _idx = 0;
        _title = @"";
        _date = @"";
        _startTime = @"";
        _endTime = @"";
        _location = @"";
        _detail = @"";
        _status = 0;
        _photoUrl = @"";
        _organiser = @"";
        
    }
    
    return self;
}

@end
