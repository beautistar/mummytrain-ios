//
//  AppDelegate.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 31/05/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    MBProgressHUD *HUD;
    UserEntity *Me;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UserEntity * Me;


// check the current network state
//-(BOOL) connected;

// show loading state when we connect to server
- (void) showLoadingViewWithTitle:(NSString *) title sender:(id) sender;

// hide loading view
-(void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay;

// show alert dialog
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender;


@end

