//
//  ActivityCell.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"
#import "UserEntity.h"

@interface ActivityCell : UITableViewCell {
    
    UserEntity *_user;
}


@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

- (void) setEvent : (EventEntity *) event from : (int) _from;
@end
