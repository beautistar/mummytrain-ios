//
//  GuestCell.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/07/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface GuestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@property (weak, nonatomic) IBOutlet UILabel *lblName;

- (void) setGuest : (UserEntity *) user;

@end
