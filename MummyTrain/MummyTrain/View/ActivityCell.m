//
//  ActivityCell.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "ActivityCell.h"
#import "CommonUtils.h"

@implementation ActivityCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setEvent : (EventEntity *) event from : (int) _from {
    
    _user = APPDELEGATE.Me;
    
    self.lblTitle.text = event._title;
    self.lblAddress.text = event._location;
    self.lblDateTime.text = event._startTime;
    
    [self.imvPhoto setImageWithURL:[NSURL URLWithString:event._photoUrl]];
    // placeholderImage:[UIImage imageNamed:@"avatar.png"]
    
    if (_from == FROM_MYEVENT) {
        
        if (event._cancelled == 1) {
            
            self.lblStatus.text = @"Cancelled";
            
        } else {
            
            if (event._user_id == _user._idx) {
                
                self.lblStatus.text = @"Hosting";
                
            } else {
                
                self.lblStatus.text = @"";
            }
        }
        
    } else {
        
        self.lblStatus.text = @"";
        
    }
}

@end
