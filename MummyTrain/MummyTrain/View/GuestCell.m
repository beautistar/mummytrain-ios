//
//  GuestCell.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/07/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "GuestCell.h"
#import "CommonUtils.h"

@implementation GuestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (void) setGuest : (UserEntity *) user {
    
    self.lblName.text = [NSString stringWithFormat:@"%@ %@", user._firstname, user._surname];
    [self.imvPhoto setImageWithURL:[NSURL URLWithString:user._photoUrl]];
    
}

@end
