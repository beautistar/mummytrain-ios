//
//  MyEventViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "MyEventViewController.h"
#import "ActivityDetailViewController.h"
#import "ActivityCell.h"
#import "UserEntity.h"
#import "EventEntity.h"
#import "CommonUtils.h"
#import "ReqConst.h"

@interface MyEventViewController () {
    
    NSMutableArray * myEventList;
    
    UserEntity *_user;
    
    EventEntity * selectedEvent;
    
}


@end

@implementation MyEventViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    FromDetail = FROM_OTHER;
    
    [super viewWillAppear:animated];
    
    [self initView];
    
    [self getMyEvent];
}

- (void) initVars {
    
    myEventList = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
}

- (void) initView {
    
    self.tabBarController.tabBar.hidden = NO;
    
    self.navigationItem.hidesBackButton = YES;
    
}

- (void) getMyEvent {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GET_MYEVENTS, _user._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [myEventList removeAllObjects];
            
            NSArray *arrEvents = [responseObject objectForKey:RES_EVENTINFOS];
            
            for(NSDictionary *dict in arrEvents) {
                
                EventEntity *event = [[EventEntity alloc] init];
                
                event._idx = [[dict valueForKey:RES_ID] intValue];
                event._user_id = [[dict valueForKey:RES_USERID] intValue];
                event._title = [dict valueForKey:RES_TITLE];
                event._date = [dict valueForKey:RES_DATE];
                event._startTime = [dict valueForKey:RES_STAETTIME];
                event._endTime = [dict valueForKey:RES_ENDTIME];
                event._location = [dict valueForKey:RES_LOCATION];
                event._status = [[dict valueForKey:RES_STATUS] intValue];
                event._detail = [dict valueForKey:RES_DETAIL];
                event._organiser = [dict valueForKey:RES_ORGANISER];
                event._cancelled = [[dict valueForKey:RES_CANCELLED] intValue];
                event._photoUrl = [dict valueForKey:RES_PHOTOURL];
                
                
                [myEventList addObject:event];
            }
            
            [self.tblMyEvent reloadData];
            
            [APPDELEGATE hideLoadingView];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
        
    }];
    
}
- (IBAction)gotoCreateAction:(id)sender {
    
    FromEdit = FROM_OTHER;
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        return [myEventList count];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"ActivityCell";
    
    // return event cell
    
    ActivityCell * cell = (ActivityCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setEvent:myEventList[indexPath.row] from:FROM_MYEVENT];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //selectedEvent = myEventList[indexPath.row];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"SegueForDetail"]) {
    
        NSIndexPath *selectedIndexPath = [self.tblMyEvent indexPathForSelectedRow];
        
        ActivityDetailViewController *destVC = (ActivityDetailViewController *) [segue destinationViewController];
        
        destVC.selectedEvent = myEventList[selectedIndexPath.row];
        destVC.fromWhere = FROM_MYEVENT;
        
    }
    
}



@end
