//
//  SettingViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "SettingViewController.h"
#import "UserEntity.h"
#import "CommonUtils.h"
#import "ReqConst.h"

@interface SettingViewController () {
    
    UserEntity *_user;
    
    NSString * photoPath;
}

@end

@implementation SettingViewController

@synthesize imvPhoto;
@synthesize txfSurName, txfFirstName, txfEmail;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
    
    [self loadUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
//    [self initView];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self initView];
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
}

- (void) initView {
    
    imvPhoto.layer.cornerRadius = imvPhoto.frame.size.height * 0.5;
    imvPhoto.layer.masksToBounds = YES;

}

- (void) loadUser {
    
    txfFirstName.text = _user._firstname;
    txfSurName.text = _user._surname;
    txfEmail.text = _user._email;
    
    [imvPhoto setImageWithURL:[NSURL URLWithString:_user._photoUrl]];
    
}

#pragma mark - Do take photo

- (IBAction)addImageAction:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openGallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];

    
    
}

- (void) openCamera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) openGallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [imvPhoto setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL) isChanged {
    
    if ([self.txfSurName.text isEqualToString:_user._surname] &&
        [self.txfFirstName.text isEqualToString:_user._firstname] &&
        [self.txfEmail.text isEqualToString:_user._email] &&
        photoPath.length == 0) {
        
        return NO;
        
    }
    

    
    return YES;
}

- (IBAction)setAction:(id)sender {
    
    if ([self isChanged]) {
        
        [self doUpdateUser];
    }
}

- (void) doUpdateUser {
    
    
    
    NSString *surname;
    
    if (self.txfFirstName.text.length == 0 ) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_FIRSTNAME positive:ALERT_OK negative:nil sender:self];
        
        return;
        
    } else if (self.txfEmail.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_EMAIL positive:ALERT_OK negative:nil sender:self];
        
        return;
        
    } else if (![CommonUtils isValidEmail:txfEmail.text]) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_CORRECT_EMAIL positive:ALERT_OK negative:nil sender:self];
        return;
    }
    
    if (self.txfSurName.text.length == 0) {
        
         surname = @"nan";
        
    } else {
        
        surname = [txfSurName.text encodeString:NSUTF8StringEncoding];
    }
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString *firstname = [txfFirstName.text encodeString:NSUTF8StringEncoding];
    
    NSString *email = [txfEmail.text encodeString:NSUTF8StringEncoding];

    
    NSString *url = [NSString stringWithFormat:@"%@%@/%d/%@/%@/%@", SERVER_URL, REQ_UPDATEUSER, _user._idx, firstname, surname, email];
    
    NSLog(@"register url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"register response : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            _user._firstname = txfFirstName.text;
            _user._surname = txfSurName.text;
            _user._email = txfEmail.text;
            
            [CommonUtils setUserRegister:YES];
            
            [CommonUtils saveUserInfo];
            
            if (photoPath.length != 0) {
                
                  [self doUploadPhoto];
                
            } else {
                
                [APPDELEGATE hideLoadingView];
            }
          
            
        } else if (result_code == CODE_EXISTUSERNAME){
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:EXIST_USERNAME positive:ALERT_OK negative:nil sender:self];
            
        } else if (result_code == CODE_EXISTEMAIL) {
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:EXIST_EMAIL positive:ALERT_OK negative:nil sender:self];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
}

- (void) doUploadPhoto {
    
    // upload profile image
    NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPLOAD_PHOTO];
    NSDictionary *params = @{
                             PARAM_ID : [NSNumber numberWithInt:_user._idx]
                             };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [APPDELEGATE hideLoadingView];
                          
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          [APPDELEGATE hideLoadingView];
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == 0) {
                              
                              NSString *photo_url = [responseObject valueForKey:RES_PHOTOURL];
                              
                              _user._photoUrl = photo_url;
                              
                              [CommonUtils saveUserInfo];
                              
                              //NSLog(@"photo : %@", [CommonUtils getPhotoUrl]);
                              
                              [[JLToast makeText:UPDATE_SUCCESS duration:2] show];
                              
                          } else {
                              
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];
    
}

- (IBAction)logoutAction:(id)sender {
    
    [CommonUtils setUserRegister:NO];    
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
