//
//  LoginViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 07/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface LoginViewController : UIViewController {
    
    UserEntity *_user;
}

@property (weak, nonatomic) IBOutlet UITextField *txfEmail;

@property (weak, nonatomic) IBOutlet UITextField *txfPassword;

@end
