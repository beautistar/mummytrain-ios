//
//  CreateEventViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 29/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"

@interface CreateEventViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) EventEntity * EditEvent;

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@property (weak, nonatomic) IBOutlet UITextField *txfTitle;

@property (weak, nonatomic) IBOutlet UITextField *txfTime;

@property (weak, nonatomic) IBOutlet UITextField *txfLocation;

@property (weak, nonatomic) IBOutlet UITextView *txvDetail;

@property (weak, nonatomic) IBOutlet UIButton *btnCancelEvent;

@property (weak, nonatomic) IBOutlet UITableView *tblLocationList;



@end
