//
//  MyEventViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyEventViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblMyEvent;

@end
