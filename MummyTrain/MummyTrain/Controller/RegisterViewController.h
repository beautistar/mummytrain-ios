//
//  RegisterViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 31/05/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface RegisterViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@property (weak, nonatomic) IBOutlet UITextField *txfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txfSurName;
@property (weak, nonatomic) IBOutlet UITextField *txfEmail;
@property (weak, nonatomic) IBOutlet UITextField *txfPassword;

@end
