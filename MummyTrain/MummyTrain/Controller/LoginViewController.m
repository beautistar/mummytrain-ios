//
//  LoginViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 07/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonUtils.h"
#import "ReqConst.h"


@implementation LoginViewController

@synthesize txfEmail, txfPassword;


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    BOOL isRegister = [CommonUtils getUserRegister];
    
    if (isRegister) {
        
        [self gotoMainTabVC];
    }    
}

- (void) gotoMainTabVC {
    
    UITabBarController *mainTab = (UITabBarController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabbar"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:mainTab];
}

- (BOOL) isValid {
    
    if (txfEmail.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_EMAIL positive:ALERT_OK negative:nil sender:self];
        
        return NO;
    } else if (txfPassword.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:ALERT_OK negative:nil sender:self];
        
        return NO;
    }
    
    return YES;
}

- (IBAction)loginAction:(id)sender {
    
    if ([self isValid]) {
        
        [self doLogin];
    }
}

- (void) doLogin {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString *email = [txfEmail.text encodeString:NSUTF8StringEncoding];
    NSString *password = [txfPassword.text encodeString:NSUTF8StringEncoding];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_LOGIN, email, password];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"login response : %@", responseObject);
        
        [APPDELEGATE hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSDictionary *dict = [responseObject objectForKey:RES_USERINFO];
            
            _user._idx = [[dict valueForKey:RES_ID] intValue];
            _user._firstname = [dict valueForKey:RES_FIRSTNAME];
            _user._surname = [dict valueForKey:RES_SURNAME];
            _user._email = [dict valueForKey:RES_EMAIL];
            _user._photoUrl = [dict valueForKey:RES_PHOTOURL];
            
            [CommonUtils setUserRegister:YES];
            
            [CommonUtils saveUserInfo];
            
            [self gotoMainTabVC];
            
        } else if (result_code == CODE_UNREGISTEREMAIL){
            
            [APPDELEGATE showAlertDialog:nil message:UNREGISTERED positive:ALERT_OK negative:nil sender:self];
            
        } else if (result_code == CODE_WRONGPWD) {
            
            
            [APPDELEGATE showAlertDialog:nil message:WRONG_PASSWORD positive:ALERT_OK negative:nil sender:self];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
    
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == txfEmail) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -20);
        [self.view setTransform:transform];
        
        [UIView commitAnimations];
    
    } else if (textField == txfPassword) {

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -30);
        [self.view setTransform:transform];
        
        [UIView commitAnimations];
        
    }
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, 0.0);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == txfEmail) {
        
        [txfPassword becomeFirstResponder];
        
    }
    
    [textField resignFirstResponder];
    
    return YES;
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
