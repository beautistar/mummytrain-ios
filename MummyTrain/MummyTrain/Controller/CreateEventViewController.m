//
//  CreateEventViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 29/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "CreateEventViewController.h"
#import "ActivityDetailViewController.h"
#import "MyEventViewController.h"
#import "CommonUtils.h"
#import "UITextView+Placeholder.h"
#import "ReqConst.h"
#import "EventEntity.h"


@interface CreateEventViewController () {
    
    UserEntity      *_user;
    NSString        *photoPath;
    EventEntity     *_newEvent;
    NSMutableArray  *_arraySuggestionLocation;
    BOOL            _isSearching;
    BOOL            _isChangedCityText;
    int             _event_id;
}

@end


@implementation CreateEventViewController

@synthesize txfTitle, txfTime, txfLocation, txvDetail;
@synthesize imvPhoto, tblLocationList;
@synthesize EditEvent;

#pragma mark - View lifeCyle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initVars];
    
    [self initView];
    
    self.navigationItem.rightBarButtonItem.title = @"Create";
    self.btnCancelEvent.hidden = YES;
    
    if (FromEdit == FROM_EDIT) {
        
        self.navigationItem.rightBarButtonItem.title = @"Update";
        
        if (EditEvent._cancelled == 1) {
            
            self.btnCancelEvent.hidden = YES;
        }
         
        self.btnCancelEvent.hidden = NO;
        
        FromEdit = 0;
        
        [self initWithEditEvent];
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    
    [self setDateTime];
    
    if (FromDetail == FROM_DETAIL) {
        
        [self initView];
        
        FromDetail = 0;
    }
    
    [self.view endEditing:YES];
}

- (void) initVars {
    
    SelStartTime = @"";
    SelEndTime = @"";
    _event_id = 0;
    
    _user = APPDELEGATE.Me;
    
    _isSearching = NO;
    _isChangedCityText = NO;
    
    _arraySuggestionLocation = [[NSMutableArray alloc] init];
    
    [txfLocation addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [tblLocationList setHidden: YES];   
    
}

- (void) initView {
    
    txfTitle.text = @"";
    txfTime.text = @"";
    txfLocation.text = @"";
    txvDetail.text= @"";
    photoPath = @"";
    [_arraySuggestionLocation removeAllObjects];
    [imvPhoto setImage:[UIImage imageNamed:@"event"]];
    
    self.tabBarController.tabBar.hidden = NO;
    
    self.btnCancelEvent.hidden = YES;
    
    txvDetail.placeholder = @"Description";
    
    [tblLocationList setHidden:YES];
    
}

- (void) initWithEditEvent {
    
    _event_id = EditEvent._idx;
    
    txfTitle.text = EditEvent._title;
    SelStartTime = EditEvent._startTime;
    SelEndTime = EditEvent._endTime;
    [self setDateTime];
    
    txfLocation.text = EditEvent._location;
    txvDetail.text = EditEvent._detail;
    
    [self.imvPhoto setImageWithURL:[NSURL URLWithString:EditEvent._photoUrl]];
}

- (void) setDateTime {
    
    if (SelStartTime.length != 0 && SelEndTime.length != 0) {
            
       txfTime.text = [NSString stringWithFormat:@"%@%@%@", SelStartTime, @" - ", SelEndTime];
    }
}


#pragma mark - button actions


- (IBAction)createAction:(id)sender {
    
    if ([self isValid]) {
        
        [self addNewEvent];
    }
}

- (IBAction)cancelAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addPhotoAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [self getPhoto];
}

- (IBAction)goDateTimeAction:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction)cancelEventAction:(id)sender {
    
    UIAlertView *altView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                      message:CONF_CANCEL
                                                     delegate:self
                                            cancelButtonTitle:ALERT_CANCEL
                                            otherButtonTitles:ALERT_OK, nil
                            ];
    [altView show];
    
    
}

# pragma mark - alert view delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (buttonIndex == 1) {
        
        [self doCancelEvent];
    }
    
}

#pragma mark - main function

- (BOOL) isValid {
    
    if (txfTitle.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_EVENTTITLE positive:ALERT_OK negative:nil sender:self];
        return NO;
        
    } else if (txfTime.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:SELECT_STARTTIME positive:ALERT_OK negative:nil sender:self];
        return NO;
        
    } else  if (txfLocation.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:SELECT_ENDTIME positive:ALERT_OK negative:nil sender:self];
        return NO;
        
    }
    
    return YES;
}

- (void) addNewEvent {    
    
    NSLog(@"%d", _event_id);
    NSLog(@"%d", _user._idx);
    NSLog(@"%@", txfTitle.text);
    NSLog(@"%@", SelStartTime);
    NSLog(@"%@", SelEndTime);
    NSLog(@"%@", txfLocation.text);
    NSLog(@"%@", txvDetail.text);
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_ADD_NEWEVENT];

    
    NSDictionary *params = @{
                             PARAM_EVENTID : [NSNumber numberWithInt:_event_id],
                             PARAM_ID : [NSNumber numberWithInt:_user._idx],
                             PARAM_TITLE : [txfTitle.text encodeString:NSUTF8StringEncoding],
                             PARAM_STARTTIME : [SelStartTime encodeString:NSUTF8StringEncoding],
                             PARAM_ENDTIME : [SelEndTime encodeString:NSUTF8StringEncoding],
                             PARAM_LOCATION : [txfLocation.text encodeString:NSUTF8StringEncoding],
                             PARAM_DETAIL : [txvDetail.text encodeString:NSUTF8StringEncoding]
                             };
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        NSLog(@"%@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            _newEvent = [[EventEntity alloc] init];
            
            _newEvent._idx = [[responseObject valueForKey:RES_ID] intValue];
            _newEvent._title = txfTitle.text;
            _newEvent._startTime = SelStartTime;
            _newEvent._endTime = SelEndTime;
            _newEvent._location = txfLocation.text;
            _newEvent._detail = txvDetail.text;
            _newEvent._organiser = _user._firstname;
            _newEvent._status = 1;
            
            if (photoPath.length != 0) {
                
                [self addNewEventPhoto];
            }
            
            else {
                
                [APPDELEGATE hideLoadingView];
                
                [[JLToast makeText:NEW_EVENT_CREATED duration:2] show];
                
                [self gotoMyEventList];
                
            }
            
        } else if (nResultCode == START_TIME_ERROR) {
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
            
        } else if (nResultCode == END_TIME_ERROR) {
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
        
        } else if (nResultCode == START_END_TIME_ERROR){
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
            
        } else {
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
    
    
}

- (void) addNewEventPhoto {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_ADD_NEWEVENTPHOTO];
    
    NSDictionary *params = @{
                             PARAM_ID : [NSNumber numberWithInt:_newEvent._idx]
                             };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [APPDELEGATE hideLoadingView];
                          
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          [APPDELEGATE hideLoadingView];
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == CODE_SUCCESS) {
                              
                              _newEvent._photoUrl = [responseObject valueForKey:RES_PHOTOURL];
                              
                              [[JLToast makeText:NEW_EVENT_CREATED duration:2] show];
                              
                              //[self gotoMyEventDetail];
                              
                              [self gotoMyEventList];
                              
                              
                          } else {
                              
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];
}

- (void) doCancelEvent {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_CANCEL_EVENT, _user._idx, EditEvent._idx];
    
    NSLog(@"cancel event url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        
        NSLog(@"cancel event response : %@", responseObject);
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (nResult_Code == CODE_SUCCESS) {
            
            [APPDELEGATE hideLoadingView];
            
            [self gotoMyEventList];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
    
}

- (void) gotoMyEventDetail {
    
    ActivityDetailViewController *detailVC = (ActivityDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityDetailViewController"];
    
    detailVC.selectedEvent = _newEvent;
    
    [self.navigationController pushViewController:detailVC animated:YES];
    
}

- (void) gotoMyEventList {
    
    MyEventViewController *destVC = (MyEventViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MyEventViewController"];
    
    [self.navigationController pushViewController:destVC animated:YES];
    
}


// custom functions

- (void) getPhoto {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openGallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) openCamera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) openGallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                [tblLocationList setHidden:YES];
                
                // update ui (set profile image with saved Photo URL
                [imvPhoto setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.tblLocationList setHidden:YES];
    
}

#pragma mark Load Suggestion Locations
- (void) loadSuggestionLocationWithString:(NSString *)string
{
    
    if (!_isSearching) {
        _isSearching = YES;
        _isChangedCityText = NO;
        
        AFHTTPSessionManager *sessionManger = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://maps.googleapis.com"]];
        NSString *requestURLString = @"/maps/api/geocode/json";
        NSDictionary *params = @{@"address" : string};
        [sessionManger GET:requestURLString parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            _isSearching = NO;
            NSLog(@"%@", responseObject);
            if ([responseObject objectForKey:@"results"]) {
                NSArray *result = responseObject[@"results"];
                [_arraySuggestionLocation removeAllObjects];
                [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    id formattedAddress = obj[@"formatted_address"];
                    [_arraySuggestionLocation addObject:formattedAddress];
                }];
                
                [self.tblLocationList setHidden:(_arraySuggestionLocation.count == 0)];
                
                [self.tblLocationList reloadData];
            }
            if (_isChangedCityText) {
                if (txfLocation.text.length > 2) {
                    [self loadSuggestionLocationWithString:self.txfLocation.text];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            _isSearching = NO;
        }];
        
    }
}

#pragma mark - UITableView datasource & delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arraySuggestionLocation.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"locationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = _arraySuggestionLocation[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    txfLocation.text = _arraySuggestionLocation[indexPath.row];
    [tableView setHidden:YES];
    [_arraySuggestionLocation removeAllObjects];
}

#pragma mark - textField delegate

- (void) textFieldDidChange:(id)textfield
{
    _isChangedCityText = YES;
    if (txfLocation.text.length > 2) {
        [self loadSuggestionLocationWithString:txfLocation.text];
    }
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(txfLocation == textField) {
        
        [txvDetail becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == txfLocation) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -230);
        [self.view setTransform:transform];
        
        [UIView commitAnimations];        
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, 0.0);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];
    
}


#pragma mark - textView delegate

- (void) textViewDidBeginEditing:(UITextView *)textView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -250);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];
}

- (void) textViewDidEndEditing:(UITextView *)textView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, 0.0);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}


@end
