//
//  DateTimeViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 26/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "DateTimeViewController.h"
#import "Const.h"
#import "CommonUtils.h"

@interface DateTimeViewController () {
    
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    
    NSDate *selectedDate;
    
    NSDateFormatter *dateFormater;
    
    NSString * _selectedTime;
    
    NSString * selDateTime;
    
    NSString * _selectedDate;
    
    int selectedTimeLbl;
    
    NSString * reqStartTime, * reqEndTime, *reqDateTime;
    
}

@end

@implementation DateTimeViewController

@synthesize lblStartTime, lblEndTime;
@synthesize btnStartTime, btnEndTime;

#pragma mark - init view

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initVars];
    
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = YES;
    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    
}

- (void) initVars {
    
    selectedTimeLbl = START_TIME_SELECT;
}

- (void) initView {
    
    //select the start time button border by default.
    btnStartTime.layer.borderWidth = 1.0f;
    btnStartTime.layer.borderColor = [[UIColor blueColor] CGColor];
    
    btnEndTime.layer.borderWidth = 0.0f;
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:_todayDate];
    
    // init time picker to today
    
    dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"hh:mm:a"];
    NSString *curTime = [dateFormater stringFromDate:self.timePicker.date];
    
    
    // date time formatting
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"MMMM d"];
    
    NSString *selToday = [formatter stringFromDate:_todayDate];
    
    selDateTime = [NSString stringWithFormat:@"%@%@%@", selToday, @", ", curTime];
    self.lblStartTime.text = selDateTime;
    
    _selectedTime = curTime;
    _selectedDate = selToday;
    
    // add action to the time picker
    [self.timePicker addTarget:self action:@selector(timeChanged:) forControlEvents:UIControlEventValueChanged];

}


- (void) timeChanged : (id) sender {
    
    _selectedTime = [dateFormater stringFromDate:self.timePicker.date];
    
    if (selectedTimeLbl == START_TIME_SELECT) {
        
        self.lblStartTime.text = [NSString stringWithFormat:@"%@%@%@", _selectedDate, @", ", _selectedTime];
        
    } else {
        
        self.lblEndTime.text = [NSString stringWithFormat:@"%@%@%@", _selectedDate, @", ", _selectedTime];

    }
}

#pragma mark - button actions

- (IBAction)cancelAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)okAction:(id)sender {
    
    SelStartTime = (NSString *)lblStartTime.text;
    SelEndTime = (NSString *)lblEndTime.text;
    
    if (SelStartTime.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:SELECT_STARTTIME positive:ALERT_OK negative:nil sender:self];
        
        return;
        
    } else if (SelEndTime.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:SELECT_ENDTIME positive:ALERT_OK negative:nil sender:self];
        
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];    
}

- (IBAction)startTimeAction:(id)sender {
    
    selectedTimeLbl = START_TIME_SELECT;
    
    btnStartTime.layer.borderWidth = 1.0f;
    btnStartTime.layer.borderColor = [[UIColor blueColor] CGColor];
    
    btnEndTime.layer.borderWidth = 0.0f;
    
}

- (IBAction)endTimeAction:(id)sender {
    
    selectedTimeLbl = END_TIME_SELECT;
    
    btnStartTime.layer.borderWidth = 0.0f;
    
    btnEndTime.layer.borderWidth = 1.0f;
    btnEndTime.layer.borderColor = [[UIColor blueColor] CGColor];
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(selectedDate && [_calendarManager.dateHelper date:selectedDate isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [UIColor blackColor];
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    selectedDate = dayView.date;
    
    
    NSLog(@"select day = %@", selectedDate);
    
    // date formatting for show
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"MMMM d"];
    
    _selectedDate = [formatter stringFromDate:selectedDate];
    
    NSLog(@"%@", _selectedDate);
    
    if (selectedTimeLbl == START_TIME_SELECT) {
        
        self.lblStartTime.text = [NSString stringWithFormat:@"%@%@%@", _selectedDate, @", ", _selectedTime];
        
        reqStartTime = reqDateTime;
    
    } else {
        
        self.lblEndTime.text = [NSString stringWithFormat:@"%@%@%@", _selectedDate, @", ", _selectedTime];
        
        reqEndTime = reqDateTime;
    }
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Previous page loaded");
}


- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0) {
        return YES;
    }
    
    return NO;
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
   
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-6];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:6];
}

@end
