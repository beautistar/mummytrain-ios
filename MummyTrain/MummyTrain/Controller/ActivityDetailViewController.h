//
//  ActivityDetailViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 02/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"

@interface ActivityDetailViewController : UIViewController

@property (nonatomic, strong) EventEntity * selectedEvent;

@property (nonatomic) int fromWhere;

@property (weak, nonatomic) IBOutlet UIView *vScrSub;

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDay;

@property (weak, nonatomic) IBOutlet UILabel *lblMonth;

@property (weak, nonatomic) IBOutlet UILabel *lblOrganizer;

@property (weak, nonatomic) IBOutlet UILabel *lblCancelled;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@property (weak, nonatomic) IBOutlet UIImageView *imvEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblEdit;

@property (weak, nonatomic) IBOutlet UILabel *lblGoing;
@property (weak, nonatomic) IBOutlet UIImageView *imvGoing;

@property (weak, nonatomic) IBOutlet UILabel *lblMaybe;
@property (weak, nonatomic) IBOutlet UIImageView *imvMaybe;

@property (weak, nonatomic) IBOutlet UIImageView *imvCantgo;
@property (weak, nonatomic) IBOutlet UILabel *lblCantgo;


@property (weak, nonatomic) IBOutlet UIButton *btnGotoGuest;

@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;

@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;

@property (weak, nonatomic) IBOutlet UILabel *lblLocation;

@property (weak, nonatomic) IBOutlet UILabel *lblGoingCount;

@property (weak, nonatomic) IBOutlet UILabel *lblMaybeCount;

@property (weak, nonatomic) IBOutlet UILabel *lblCantGoCount;

@property (weak, nonatomic) IBOutlet UIImageView *imvWrittingUserPhoto;

@property (weak, nonatomic) IBOutlet UITextView *txvWriteSomething;

@property (weak, nonatomic) IBOutlet UIView *vForEdit;

@property (weak, nonatomic) IBOutlet UIView *vComment;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vScrCntHeight;

@end
