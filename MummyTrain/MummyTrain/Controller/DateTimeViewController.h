//
//  DateTimeViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 26/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Const.h"

#import <JTCalendar/JTCalendar.h>

@interface DateTimeViewController : UIViewController <JTCalendarDelegate>

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;

@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;

@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;

@property (weak, nonatomic) IBOutlet UITextField *lblStartTime;

@property (weak, nonatomic) IBOutlet UITextField *lblEndTime;

@property (weak, nonatomic) IBOutlet UIButton *btnStartTime;

@property (weak, nonatomic) IBOutlet UIButton *btnEndTime;

@end
