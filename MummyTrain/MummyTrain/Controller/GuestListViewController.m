//
//  GuestListViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/07/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "GuestListViewController.h"
#import "GuestCell.h"
#import "Const.h"

@implementation GuestListViewController

@synthesize guestList, guests;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void) initView {
    
    guests = [[NSMutableArray alloc] init];
    
    self.lblGoing.text = [NSString stringWithFormat:  @"%d Going", _goingCnt];
    self.lblMaybe.text = [NSString stringWithFormat:  @"%d Maybe", _maybeCnt];
    self.lblCantgo.text = [NSString stringWithFormat:  @"%d Can't go", _cantgoCnt];
    
    self.lblGoing.textColor = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0];
    NSLog(@"guest count : %d", (int)guestList.count);
    
    [self setGuest:1];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


// working
- (IBAction)goingAction:(id)sender {
    

    [self setGuest:IS_GOING];
    
}

- (IBAction)maybeAction:(id)sender {
    

    [self setGuest:IS_MAYBE];
    
}

- (IBAction)cantAction:(id)sender {
    

    [self setGuest:IS_CANTGO];
    
}

- (void) setGuest : (int )status {

    UserEntity * guest;
    
    [guests removeAllObjects];
    
    for (int i = 0 ; i < guestList.count ; i++) {
        
        guest = (UserEntity *)guestList[i];
        
        if (guest._status == status) {
            
            [guests addObject:guest];
        }
    }
    
    [_tblGustList reloadData];
    
    switch (status) {
            
        case IS_GOING:
            
            self.lblGoing.textColor = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0];
            self.lblMaybe.textColor = [UIColor grayColor];
            self.lblCantgo.textColor = [UIColor grayColor];
            break;
            
        case IS_MAYBE :
            
            self.lblMaybe.textColor = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0];
            self.lblCantgo.textColor = [UIColor grayColor];
            self.lblGoing.textColor = [UIColor grayColor];
            break;
            
        case IS_CANTGO :
            
            self.lblCantgo.textColor = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0];
            self.lblGoing.textColor = [UIColor grayColor];
            self.lblMaybe.textColor = [UIColor grayColor];
            break;
    }
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [guests count];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"GuestCell";
    
    // return event cell
    
    GuestCell * cell = (GuestCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setGuest:guests[indexPath.row]];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //selectedEvent = myEventList[indexPath.row];
    
}


@end
