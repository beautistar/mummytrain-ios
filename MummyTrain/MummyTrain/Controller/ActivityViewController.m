//
//  ActivityViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActivityDetailViewController.h"
#import "ActivityCell.h"
#import "UIScrollView+DXRefresh.h"
#import "CommonUtils.h"
#import "ReqConst.h"
#import "UserEntity.h"
#import "EventEntity.h"

@interface ActivityViewController () {
    
    NSMutableArray * activityList;
    int curPage;
    
    UserEntity *_user;
    
    EventEntity * selectedEvent;
}

@end

@implementation ActivityViewController

@synthesize tblActivity;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
   
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
     [self initView];
    
    [self getAllEvents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
    activityList = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
    
    curPage = 0;
}

- (void) initView {
    
    self.tabBarController.tabBar.hidden = NO;
    
    self.tblActivity.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.tblActivity addFooterWithTarget:self action:@selector(refreshFooter) withIndicatorColor:[UIColor colorWithRed:156/255.0 green:39/255.0 blue:176/255.0 alpha:1.0]];

}


#pragma mark - main actions

- (void) getAllEvents {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    if (curPage != 0) {
        [self.tblActivity footerBeginRefreshing];
    }
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d/", SERVER_URL, REQ_GET_ALLEVENTS, _user._idx, curPage];
    
    NSLog(@"all event url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        
        NSLog(@"all events : %@", responseObject);
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSArray *arrEvents = [responseObject objectForKey:RES_EVENTINFOS];
            
            if (curPage == 0) {
                
                [activityList removeAllObjects];
            }
            
            for(NSDictionary *dict in arrEvents) {
                
                EventEntity *event = [[EventEntity alloc] init];
                
                event._idx = [[dict valueForKey:RES_ID] intValue];
                event._user_id = [[dict valueForKey:RES_USERID] intValue];
                event._title = [dict valueForKey:RES_TITLE];
                event._date = [dict valueForKey:RES_DATE];
                event._startTime = [dict valueForKey:RES_STAETTIME];
                event._endTime = [dict valueForKey:RES_ENDTIME];
                event._location = [dict valueForKey:RES_LOCATION];
                event._status = [[dict valueForKey:RES_STATUS] intValue];
                event._detail = [dict valueForKey:RES_DETAIL];
                event._organiser = [dict valueForKey:RES_ORGANISER];
                event._photoUrl = [dict valueForKey:RES_PHOTOURL];
                
                
                [activityList addObject:event];

            }
            
            [self loadDetails];
            
            [APPDELEGATE hideLoadingView];
            
            if ([arrEvents count] == 0 &&  curPage == 0) {
                [[JLToast makeText:@"No Events" duration: 2] show];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        if(self.tblActivity.isFooterRefreshing) {
            [self.tblActivity footerEndRefreshing];
        }
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
    
}

- (void) loadDetails {
    
    //[self updateEvent];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [APPDELEGATE hideLoadingView];
        
        [self.tblActivity reloadData];
        
        // end footer refreshing
        if(self.tblActivity.isFooterRefreshing) {
            [self.tblActivity footerEndRefreshing];
        }
    });
}

- (void)refreshFooter {
    
    curPage ++;
    [self getAllEvents];
}


#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [activityList count];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"ActivityCell";
    
    // return dictionary cell
    
    ActivityCell * cell = (ActivityCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setEvent:activityList[indexPath.row] from:FROM_WHATSON];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedEvent = activityList[indexPath.row];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *selectedIndexPath = [self.tblActivity indexPathForSelectedRow];
    
    ActivityDetailViewController *destVC = (ActivityDetailViewController *) [segue destinationViewController];
    
    destVC.selectedEvent = activityList[selectedIndexPath.row];
    destVC.fromWhere = FROM_WHATSON;
}







@end
