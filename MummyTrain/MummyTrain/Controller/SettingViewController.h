//
//  SettingViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@property (weak, nonatomic) IBOutlet UITextField *txfFirstName;

@property (weak, nonatomic) IBOutlet UITextField *txfSurName;

@property (weak, nonatomic) IBOutlet UITextField *txfEmail;

@end
