//
//  ActivityDetailViewController.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 02/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "CreateEventViewController.h"
#import "GuestListViewController.h"
#import "CommentEntity.h"
#import "DLRadioButton.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "ReqConst.h"
#import "UITextView+Placeholder.h"

@interface ActivityDetailViewController () {
    
    int attendState;
    UserEntity *_user;
    NSMutableArray *allUsers;
    int goingCnt, maybeCnt, cantgoCnt;
    
    NSMutableArray *_comments;
}

@end

@implementation ActivityDetailViewController

@synthesize lblTitle;

@synthesize imvPhoto;
@synthesize selectedEvent;
@synthesize vForEdit;
@synthesize fromWhere;

#pragma mark - view init & lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
    
    [self loadEventDetail];
    
    [self getAllUsers];
    
    [self getComment];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
    attendState = IS_DEFAULT;
    
    allUsers = [[NSMutableArray alloc] init];
    
    _comments = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
}

- (void) initView {
    
    self.tabBarController.tabBar.hidden = YES;
    
    self.txvWriteSomething.text = @"";
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSLog(@"%d", fromWhere);
    
    // User can only edit what selected event is come from My Event and it was upload by me
    
    if (fromWhere == FROM_WHATSON) {
        
        NSLog(@"ste : %d", selectedEvent._status);
        
        vForEdit.hidden = YES;
        
        //[self setAttendStatus : selectedEvent._status];
        
        self.btnGotoGuest.enabled = NO;
        
    } else if (fromWhere == FROM_MYEVENT && selectedEvent._user_id == _user._idx) {
        
        vForEdit.hidden = NO;
        
        self.btnGotoGuest.enabled = YES;
        
    } else {
        
        vForEdit.hidden = YES;
        
        self.btnGotoGuest.enabled = NO;
        
        [self setAttendStatus : selectedEvent._status];
    }
    
    self.txvWriteSomething.text = @"";
    self.txvWriteSomething.placeholder = @"Write Something";
}

#pragma mark - loading view

- (void) loadEventDetail {
    
    self.navigationItem.title =selectedEvent._title;
    self.lblTitle.text = selectedEvent._title;
    self.lblOrganizer.text = [NSString stringWithFormat:@"Hosted by %@", selectedEvent._organiser];
    self.lblStartTime.text = selectedEvent._startTime;
    self.lblEndTime.text = selectedEvent._endTime;
    self.lblLocation.text = selectedEvent._location;
    
    if (selectedEvent._cancelled == 1) {
        
        self.lblCancelled.hidden = NO;
        
        [self.btnEdit setEnabled:NO];
        
        [self.imvEdit setImage:[UIImage imageNamed:@"d_edit"]];

        self.lblEdit.textColor = [UIColor grayColor];
//        self.lblEdit.textColor = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0];        
        
    } else {
        
        self.lblCancelled.hidden = YES;
    }
    
    // for month with 3 upper letter
    NSString *_month = [selectedEvent._startTime substringWithRange:NSMakeRange(0, 3)];
    self.lblMonth.text = [_month uppercaseString];
    
    // for day label
    NSArray *arr1 = [selectedEvent._startTime componentsSeparatedByString:@", "];
    NSArray *arr2 = [[arr1 objectAtIndex:0] componentsSeparatedByString:@" "];
    NSString *day = [arr2 objectAtIndex:1];
    
    self.lblDay.text = day;
    
    [self.imvPhoto setImageWithURL:[NSURL URLWithString:selectedEvent._photoUrl]];
    
    [self.imvWrittingUserPhoto setImageWithURL:[NSURL URLWithString:_user._photoUrl]];
    
}

#pragma mark - main APIs

- (void) getAllUsers {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_GET_USERINFO, _user._idx, selectedEvent._idx];
    
    NSLog(@"get atending user info request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"get attend users : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [allUsers removeAllObjects];
            
            NSArray *arrUsers = [responseObject objectForKey:RES_USERINFOS];
            
            for(NSDictionary *dict in arrUsers) {
                
                UserEntity *user = [[UserEntity alloc] init];
                
                user._idx = [[dict valueForKey:RES_ID] intValue];
                user._surname = [dict valueForKey:RES_SURNAME];
                user._firstname= [dict valueForKey:RES_FIRSTNAME];
                user._email = [dict valueForKey:RES_EMAIL];
                user._photoUrl = [dict valueForKey:RES_PHOTOURL];
                user._status = [[dict valueForKey:RES_STATUS] intValue];
                
                [allUsers addObject:user];
            }
            
            [self setAllUsers];
            
            [APPDELEGATE hideLoadingView];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
        
    }];
    
}

- (void) setAllUsers {
    
    goingCnt = 0;
    maybeCnt = 0;
    cantgoCnt = 0;
    
    if (allUsers.count > 0) {
    
        for (int i = 0 ; i < (int)allUsers.count ; i++) {
            
            UserEntity *_whoUser = allUsers[i];
            
            switch (_whoUser._status) {
                
                case IS_GOING:
                    
                    goingCnt++;
                    break;
                    
                case IS_MAYBE:
                    
                    maybeCnt++;
                    break;
                    
                case IS_CANTGO:
                    
                    cantgoCnt++;
                    break;
            }
        }
    }

    self.lblGoingCount.text = [NSString stringWithFormat:@"%d", goingCnt];
    self.lblMaybeCount.text = [NSString stringWithFormat:@"%d", maybeCnt];
    self.lblCantGoCount.text = [NSString stringWithFormat:@"%d", cantgoCnt];
    
}

- (void) setComment {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString *comment = [self.txvWriteSomething.text encodeString:NSUTF8StringEncoding];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d/%@", SERVER_URL, REQ_SETCOMMENT, _user._idx, selectedEvent._idx, comment];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"set comment response : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [APPDELEGATE hideLoadingView];
            
            self.txvWriteSomething.text = @"";
            [self.view endEditing:YES];
            
            [self getComment];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
        
    }];
}

- (void) getComment {
  
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETCOMMENT, selectedEvent._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"get comments : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [_comments removeAllObjects];
            
            NSArray *arrComments = [responseObject objectForKey:RES_COMMENT_INFOS];
            
            for(NSDictionary *dict in arrComments) {
                
                CommentEntity *comment = [[CommentEntity alloc] init];
                
                comment._idx = [[dict valueForKey:RES_ID] intValue];
                comment._photoUrl = [dict valueForKey:RES_PHOTOURL];
                comment._comment = [dict valueForKey:RES_COMMENT];
                comment._dateTime = [dict valueForKey:RES_DATE];
                
                [_comments addObject:comment];
            }
            
            [self setCommentViews];
            
            [APPDELEGATE hideLoadingView];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
        
    }];
    
}

- (void) setCommentViews {
    
    //init value
    self.vScrCntHeight.constant = -225;
    
    for (int i = 0 ; i < _comments.count ; i++) {
        
        CommentEntity * _comment = _comments[i];
        
        UIView *commentView = [[UIView alloc] initWithFrame:CGRectMake(0, 160 + self.vComment.frame.origin.y + (i + 1) * self.vComment.frame.size.height, self.view.frame.size.width, 80)];
        
        commentView.backgroundColor = [UIColor whiteColor];
        
        [self.vScrSub addSubview:commentView];
        
        //user image
        UIImageView * imvCommentUserPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(30, commentView.frame.origin.y + 4, 72, 72)];
        [imvCommentUserPhoto setImageWithURL:[NSURL URLWithString:_comment._photoUrl]];
        
        //comment textbox
        UITextView *txvComment = [[UITextView alloc] initWithFrame:CGRectMake(imvCommentUserPhoto.frame.origin.x + imvCommentUserPhoto.frame.size.width + 10, commentView.frame.origin.y + 4, commentView.frame.size.width - imvCommentUserPhoto.frame.origin.x - imvCommentUserPhoto.frame.size.width - 20 - 50, 72)];
        
        txvComment.backgroundColor = [UIColor whiteColor];
        txvComment.text = _comment._comment;
        
        //date time label
        UILabel * lblDateTime = [[UILabel alloc] initWithFrame:CGRectMake(txvComment.frame.origin.x + txvComment.frame.size.width + 5, commentView.frame.origin.y + 4, 35, 72)];
        
        lblDateTime.numberOfLines = 2;
        lblDateTime.textAlignment = UIViewContentModeCenter;
        [lblDateTime setFont:[UIFont systemFontOfSize:11]];
        lblDateTime.textColor = [UIColor grayColor];
        lblDateTime.text = _comment._dateTime;


        [self.vScrSub addSubview:imvCommentUserPhoto];
        [self.vScrSub addSubview:txvComment];
        [self.vScrSub addSubview:lblDateTime];
        
        self.vScrCntHeight.constant += 80;
    }
}

#pragma mark - button actions

- (IBAction) backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    FromDetail = FROM_DETAIL;
}

- (IBAction)editAction:(id)sender {
    
    FromEdit = FROM_EDIT;
}
- (IBAction)gotoGuestAction:(id)sender {
    
}

- (IBAction)goingAction:(id)sender {
    
    [self setAttendStatus:IS_GOING];
    
    attendState = IS_GOING;
    
    [self doAttend];
}


- (IBAction)maybeAction:(id)sender {
    
    [self setAttendStatus:IS_MAYBE];
    
    attendState = IS_MAYBE;
    
    [self doAttend];
}


- (IBAction)cantgoAction:(id)sender {
    
    [self setAttendStatus:IS_CANTGO];
    
    attendState = IS_CANTGO;
    
    [self doAttend];
}

- (IBAction)commentAction:(id)sender {
    
    if (self.txvWriteSomething.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:WRITE_SONETHING positive:ALERT_OK negative:nil sender:self];
        
        return;
    } else {
        
        [self setComment];
    }
    
}

#pragma mark - Custom Func

- (void) setAttendStatus : (int) status {
    
    switch (status) {
            
        case IS_GOING:
            
            self.lblGoing.textColor = [UIColor colorWithRed:2/225.0 green:136/225.0 blue:209/225.0 alpha:1.0];
            self.lblMaybe.textColor = [UIColor grayColor];
            self.lblCantgo.textColor = [UIColor grayColor];
            
            [self.imvGoing setImage:[UIImage imageNamed:@"Sgoing"]];
            [self.imvMaybe setImage:[UIImage imageNamed:@"maybe"]];
            [self.imvCantgo setImage:[UIImage imageNamed:@"notGoing"]];
            
            break;
            
            
        case IS_MAYBE:
            
            self.lblMaybe.textColor = [UIColor colorWithRed:2/225.0 green:136/225.0 blue:209/225.0 alpha:1.0];
            self.lblGoing.textColor = [UIColor grayColor];
            self.lblCantgo.textColor = [UIColor grayColor];
            
            [self.imvGoing setImage:[UIImage imageNamed:@"going"]];
            [self.imvMaybe setImage:[UIImage imageNamed:@"Smaybe"]];
            [self.imvCantgo setImage:[UIImage imageNamed:@"notGoing"]];
            
            break;
            
        case IS_CANTGO :
            
            self.lblCantgo.textColor = [UIColor colorWithRed:2/225.0 green:136/225.0 blue:209/225.0 alpha:1.0];
            self.lblGoing.textColor = [UIColor grayColor];
            self.lblMaybe.textColor = [UIColor grayColor];
            
            [self.imvGoing setImage:[UIImage imageNamed:@"going"]];
            [self.imvMaybe setImage:[UIImage imageNamed:@"maybe"]];
            [self.imvCantgo setImage:[UIImage imageNamed:@"SnotGoing"]];
            
            break;
            
        case -1 :
            
            self.lblGoing.textColor = [UIColor grayColor];
            self.lblMaybe.textColor = [UIColor grayColor];
            self.lblCantgo.textColor = [UIColor grayColor];
            
            [self.imvGoing setImage:[UIImage imageNamed:@"going"]];
            [self.imvMaybe setImage:[UIImage imageNamed:@"maybe"]];
            [self.imvCantgo setImage:[UIImage imageNamed:@"notGoing"]];
            
            break;
    }
}

- (void) doAttend {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%d/%d/%d", SERVER_URL, REQ_SET_ATTEND,  _user._idx, selectedEvent._idx, attendState];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        [APPDELEGATE hideLoadingView];
        
        if (result_code == CODE_SUCCESS) {
            
            [[JLToast makeText:ATTEND_SUCCESS duration:2] show];
        
        } else {
            
            [APPDELEGATE showAlertDialog:ALERT_TITLE message:RES_ALREADYATTEND positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"SegueForEdit"]) {
        
        CreateEventViewController *destVC = (CreateEventViewController *) [segue destinationViewController];
        
        destVC.EditEvent = selectedEvent;
        
    } else {
        
        GuestListViewController *_destVC = (GuestListViewController *) [segue destinationViewController];
        
        _destVC.guestList = allUsers;
        _destVC.goingCnt = goingCnt;
        _destVC.maybeCnt = maybeCnt;
        _destVC.cantgoCnt = cantgoCnt;
    }
}

#pragma mark - textview delegate

- (void) textViewDidBeginEditing:(UITextView *)textView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -250);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];
    
}

- (void) textViewDidEndEditing:(UITextView *)textView {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, 0.0);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}
@end
