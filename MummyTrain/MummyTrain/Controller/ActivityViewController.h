//
//  ActivityViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 01/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblActivity;

@end
