//
//  GuestListViewController.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/07/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) int goingCnt;
@property (nonatomic) int cantgoCnt;
@property (nonatomic) int maybeCnt;
@property (nonatomic, strong) NSMutableArray * guestList;
@property (nonatomic, strong) NSMutableArray * guests;

@property (weak, nonatomic) IBOutlet UILabel *lblGoing;
@property (weak, nonatomic) IBOutlet UILabel *lblMaybe;
@property (weak, nonatomic) IBOutlet UILabel *lblCantgo;
@property (weak, nonatomic) IBOutlet UITableView *tblGustList;

@end
