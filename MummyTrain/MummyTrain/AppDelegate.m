//
//  AppDelegate.m
//  MummyTrain
//
//  Created by Astolfo Arcuri on 31/05/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonUtils.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize Me;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // initialize user info and retreive user info from user default
    Me = [[UserEntity alloc] init];
    [CommonUtils loadUserInfo];
    
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/**  Activity Indicator Utils  **/
- (void) showLoadingViewWithTitle:(NSString *) title sender:(id) sender
{
    HUD = [[MBProgressHUD alloc] initWithView:self.window]; //rootVC.navigationController.view
    
    [self.window addSubview:HUD];
    HUD.minSize = CGSizeMake(100.f, 100.f);
    
    // Set the hud to display with a color
    HUD.color = [UIColor colorWithRed:156/255.0 green:39/255.0 blue:176/255.0 alpha:0.70];
    
    HUD.delegate = sender;
    HUD.labelText = title;
    
    [HUD show:YES];
}

- (void) hideLoadingView {
    
    [HUD hide:YES];
}

- (void) hideLoadingView : (NSTimeInterval) delay {
    
    [HUD hide:YES afterDelay:delay];
}

/**  Alert Utils  **/
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender {
    
    NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:156/255.0 green:39/255.0 blue:176/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    
    NSMutableAttributedString *attributedTitle;
    
    if (title != nil) {
        attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
        [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, title.length)];
    }
    
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedMessage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:16.0] range:NSMakeRange(0, message.length)];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if(title != nil) {
        [alert setValue:attributedTitle forKey:@"attributedTitle"];
    }
    
    [alert setValue:attributedMessage forKey:@"attributedMessage"];
    
    if(strPositivie != nil) {
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositivie
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
        
        [alert addAction:yesButton];
    }
    
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strNegative
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
    }
    
    [sender presentViewController:alert animated:YES completion:nil];
    //    alert.view.tintColor = [UIColor darkGrayColor];
    //    alert.view.backgroundColor = [UIColor colorWithRed:71/255.0 green:74/255.0 blue:85/255.0 alpha:1.0];
}


@end
